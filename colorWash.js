/*jshint esversion: 6 , browser: true*/
'use strict';
// console.log(window.gsap);
if(!window.gsap){
    let script2 = document.createElement("script");
    script2.type = "text/javascript";
    script2.src = "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/gsap.min.js";
    document.getElementsByTagName("head")[0].appendChild(script2);
}


function colorWash(div_, fill){

    if(div_ === undefined || div_ === null){
        return false;
    }

    if(fill === undefined || fill === null || fill === false){
        fill = false;
    }else{
        fill = true;
    }

    var time = randNum(1, randNum(1, 40)) * 1000;
    setInterval(colorSwitch(div_, time, randClor()), time);

    function colorSwitch(elm, time, color){

        if(fill){

            gsap.to(elm, {duration: time/1000,
                          fill: randClor(),
                          overwrite:true,
                          onComplete:function(){
                                let time = randNum(1, randNum(1, 40)) * 1000;
                                colorSwitch(elm, time, randClor());
                            }
                         });
        }else{
            gsap.to(elm, {duration: time/1000,
                          backgroundColor: randClor(),
                          overwrite:true,
                          onComplete:function(){
                                let time = randNum(1, randNum(1, 40)) * 1000;
                                colorSwitch(elm, time, randClor());
                            }
                         });
        }
    }

    function randClor(colArr){
        if(colArr === undefined || Object.prototype.toString.call(colArr) !== '[object Array]'){
            let leterColor = ['A', 'B', 'C', 'D', 'E', 'F'];
            let newColor = '';
            for(let i = 6; i >= 1; i--){
                if((Math.floor(Math.random() * (1+498-1)) + 1) %2 === 0){
                    newColor = newColor + leterColor[Math.floor(Math.random() * 6)];
                }else{
                    newColor = newColor + Math.floor(Math.random() * 10);
                }
            }
            return '#' + newColor;
        }
        else{
            let newArr = Array();
            for(let color in colArr){
                if(colArr[color].match(/^#?[A-Fa-f0-9]{6}[\s]{0,1}$/g)){
                    if(colArr[color].substring(0, 1) !== '#'){
                        newArr.push('#'+colArr[color].toUpperCase());
                    }else{
                        newArr.push(colArr[color].toUpperCase());
                    }
                }
            }
            return newArr[randNum(0, newArr.length - 1)];
        }
    }

    function randNum(low, high){
        return Math.floor(Math.random() * (1+high-low)) + low;
    }
}